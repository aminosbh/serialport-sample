/*
 * Copyright (c) 2019 Amine Ben Hassouna <amine.benhassouna@gmail.com>
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <libserialport.h>

#define SERIALPORT  "/dev/ttyACM0"

int main(int argc, char* argv[])
{
    // Unused argc, argv
    (void) argc;
    (void) argv;

    struct sp_port* serialport = NULL;

    // Get serial port
    if(sp_get_port_by_name(SERIALPORT, &serialport) != SP_OK) {
        fprintf(stderr, "Error while getting the serial port %s\n", SERIALPORT);
        return 1;
    }

    // Open serial port
    if(sp_open(serialport, SP_MODE_READ_WRITE) != SP_OK) {
        fprintf(stderr, "Error while opening the serial port %s\n", SERIALPORT);
        goto cleanup;
    }

    // Configure the baudrate of the serial port to 9600
    sp_set_baudrate(serialport, 9600);


    // Clear the serial port buffer
    sp_flush(serialport, SP_BUF_BOTH);

    char msg[100];

    // Send message
    printf("Please enter a message: ");
    scanf("%s", msg);
    strcat(msg, "\n");
    sp_blocking_write(serialport, msg, strlen(msg), 100);

    // Receive message
    sp_blocking_read(serialport, msg, 100, 100);
    printf("msg: %s", msg);

    // Close serial port
    if(sp_close(serialport) != SP_OK) {
        fprintf(stderr, "Error while closing the serial port %s\n", SERIALPORT);
        goto cleanup;
    }

cleanup:
    // Free serial port
    sp_free_port(serialport);

    return 0;
}
