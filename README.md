# Serial port communication in C

This project is a basic C program that demonstrates how we can communicate over
the serial port. It uses [CMake][] as a build system.

## Dependencies

- [Git][]
- C Compiler (gcc, ...)
- [CMake][]
- [libserialport][] library

**On Debian/Ubuntu based distributions, use the following command:**

```sh
sudo apt install git build-essential pkg-config cmake cmake-data
```

**Install libserialport from source:**

```sh
git clone git://sigrok.org/libserialport
cd libserialport
./autogen.sh
./configure
make
sudo make install
sudo ldconfig
```

## Build instructions

```sh
# Clone this repo
git clone https://gitlab.com/aminosbh/serialport-sample.git
cd serialport-sample

# Create a build folder
mkdir build
cd build

# Build
cmake ..
make

# Run
./serialport-sample
```

### Open the project with an IDE under Linux

See [IDE_USAGE.md](IDE_USAGE.md) for details.

## License

Author: Amine B. Hassouna [@aminosbh](https://gitlab.com/aminosbh)

This project is distributed under the terms of the MIT license
[&lt;LICENSE&gt;](LICENSE).



[CMake]: https://cmake.org
[Git]: https://git-scm.com
[libserialport]: https://sigrok.org/wiki/Libserialport
